#!/usr/bin/env node
'use strict';

// ** Dependencies
const Action = require('../lib/Action');
const yargs = require('yargs');
const files = require('nodus-framework').files;
const logger = require('nodus-framework').logging.createLogger('actum');

// Program Help
const argv = yargs
    .usage('Usage: $0 <command> [options]')
    .command('run <action> [args..]', 'Run an action and pass in some arguments')
    .epilog('Copyright (c) 2017 Brad Serbu <bradserbu@bradserbu.com>\nAll rights reserved.')
    .version()
    .help()
    .argv;

// Log the arguments passed to the command for debugging purposes
logger.debug('ARGV', argv);

// Check to see what command was run
const command = argv._.shift();
logger.debug('COMMAND', command);
if (!command)
    return yargs.showHelp();

// Load the action specified by the command
const action = files.requireFile(argv.action);
logger.debug('ACTION', action);

// Parse the args to pass to the function
const args = argv.args;
logger.debug('ARGS', args);

// Get the expected parameters to the action
const parameters = action.parameters;
logger.debug('PARAMETERS', action.parameters);

// TODO: Extract options to the action from argv
const options = argv;

function runAction(args) {

    // Args is an array
    return action
        .run
        .apply(null, args)
}

// Run the action
runAction(args)
    .then(result => console.log(result))
    .catch(err => console.error(err))
    .finally(() => {
        // Output the statistics
        logger.debug('STATS', action.stats);
    });