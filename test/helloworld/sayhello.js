'use strict';

function sayhello(name) {
    return `Hello, ${name}!`;
}

console.log(sayhello("Brad Serbu"));