
// ** Dependencies
const Action = require('../../lib/Action');

// Create Say Hello Action
const sayhello = Action('sayhello', (first, last) => `Hello, ${first} ${last}!`);

// ** Run Say Hello and log result
sayhello.run("Brad", "Serbu").then(result => console.log(result));