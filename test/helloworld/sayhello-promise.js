
// Create Say Hello Action
const sayhello = name => Promise.resolve(`Hello, ${name}!`);

// ** Run Say Hello and log result
sayhello("Brad Serbu").then(result => console.log(result));