'use strict';

// ** Dependencies
const Process = require('../../lib/Process');
const parseName = require('another-name-parser');

Process
    .create('last-name', 'Process to get the last name of a person.')
    .then('parse-name', name => parseName(name))
    .then('last-name', (name, input) => {
        console.log('INPUT', input);
        return name.last;
    })
    .run('Serbu, Brad')
    .then(result => console.log(result));