'use strict';

// ** Dependencies
const Action = require('./lib/Action');
const Process = require('./lib/Process');

// ** Exports
module.exports = Action;
module.exports.Action = Action;
module.exports.Process = Process;