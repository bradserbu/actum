'use strict';

// ** Dependencies
const Action = require('../../lib/Action');

// ** Exports
module.exports = Action('sayhello', (first, last) => `Hello, ${first} ${last}!`);

