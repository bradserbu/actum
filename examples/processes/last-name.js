'use strict';

// ** Dependencies
const Process = require('../../lib/Process');
const parseName = require('another-name-parser');

module.exports = Process
    .create('last-name', 'Process to get the last name of a person.')
    .then('parse-name', name => parseName(name))
    .then('last-name', (parsed, input) => name.last)