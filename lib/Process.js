'use strict';

// ** Dependencies
const _ = require('lodash');
const util = require('util');
const EventEmitter = require('eventemitter2');
const Action = require('./Action');
const measured = require('measured');
const Promise = require('bluebird');
const errors = require('nodus-framework').errors;
const logger = require('nodus-framework').logging.createLogger('actum:process');

class Process extends EventEmitter {
    constructor(name, options) {
        super();

        this._name = name;
        this._description = options.description || '';
        this._activities = [];

        this._stats = measured.createCollection();
    }

    get name() {
        return this._name;
    }

    get description() {
        return this._description;
    }

    get run() {
        const self = this;
        const activities = self._activities;
        const stats = self._stats;

        // Keep track of where we are in the list of actions
        let pos = 0;

        // Function to check if we are the last activity
        const done = () => ++pos >= activities.length;

        // Track Total Executions of the process
        stats.counter('total-calls').inc();

        const input = [];

        // Where we are in the actions array
        return function next() {

            // The current activity to process
            const current = activities[pos];

            const args = Array.prototype.slice.call(arguments);
            // logger.debug('ARGS', args);

            // Add args to the front of the previous inputs
            input.unshift.apply(input, args);
            // logger.debug('INPUT', input);

            // Run the current task
            const result = current.run.apply(current, input);

            // Return result if we are the last activity
            if (done())
                return result;

            // Continue on to the next activity
            return result.then(next);
        }
    }

    get stats() {
        const stats = this._stats.toJSON();

        // Add stats to the activities
        stats.activities = _.map(this._activities, activity => activity.stats);

        return stats;
    }

    then(name, handler, options) {

        const action = new Action(name, handler, options);

        this._activities.push(action);
        return this;
    }

    each(name, selector, handler, options) {

        // Create an action to handle a single item
        const itemAction = new Action(name, handler, options);

        const selectItemsAction = new Action(name, function () {
            logger.debug('SELECTOR_ARGS', arguments);

            if (arguments.length === 0)
            // TODO: consider doing nothing here, instead of throwing an error
                throw errors('NO_INPUT', 'There is nothing to select on the each command.');

            // Choose property as selector
            if (util.isString(selector)) {

                if (!arguments[0].hasOwnProperty(selector))
                    throw errors('PROPERTY_NOT_FOUND', 'The selector property was not found on the input', {selector: selector});

                return arguments[0][selector];
            }

            // Function to select elements
            if (util.isFunction(selector)) {
                return selector.apply(this, arguments);
            }

            throw errors('NOT_SUPPORTED', 'The selector type is not supported.', {selector: selector});
        }, options);

        // Action to process all the items
        const eachAction = new Action(`each:${name}`, (input) =>
            selectItemsAction.run(input)
            // Apply each to each item
                .then(items => {

                    // If items is a [name, value] pair and apply to the handler
                    if (util.isObject(items)) {
                        // logger.debug('Items object detected, converting to name, value pair.');
                        items = _.map(items, (value, name) => [name, value]);
                    }

                    // Run Item Action for each of the items
                    return Promise
                        .mapSeries(items, item => itemAction.run.apply(itemAction, item))
                        .then(results => {
                            return input; // Return the original input as results
                        });
                })
        );

        this._activities.push(eachAction);
        return this;
    }
}

function createProcess(name, options) {

    const process = new Process(name, options);
    return process;
}

// ** Exports
module.exports = createProcess;
module.exports.create = createProcess;
module.exports.Process = Process;