'use strict';

'use strict';

// ** Dependencies
const Promise = require('bluebird');
const EventEmitter = require('eventemitter2');
const logging = require('nodus-framework').logging;
const functions = require('nodus-framework').functions;
const measured = require('measured');

const logger = require('nodus-framework').logging.createLogger('actum');

/**
 * The id of each action instance will be an incrementing counter
 * @returns {function(): number}
 * @constructor
 */
function DEFAULT_NEXT_ID() {
    let count = 0;
    return () => ++count;
}

class Action extends EventEmitter {
    constructor(name, handler, options) {
        super();

        options = options || {};

        this._name = name;
        this._handler = handler;
        this._parameters = options.parameters || {};

        // Initialize statistics
        this._stats = measured.createCollection();
        this._nextId = options.next_id || DEFAULT_NEXT_ID();
    }

    get name() {
        return this._name;
    }

    get parameters() {
        return this._parameters;
    }

    get handler() {
        return this._handler;
    }

    get logger() {
        return this._logger;
    }

    get stats() {
        return this._stats.toJSON();
    }

    get run() {

        // Capture some variables to
        const self = this;
        const totalCalls = this._stats.counter('total-calls');
        const getInstanceId = this._nextId;

        return function () {

            // Get the ID of this instance
            const id = getInstanceId();

            // Increment stats for times this action was called
            totalCalls.inc();

            // Log for just this instance
            const instance = {
                id: id,
                args: arguments,
                action: self
            };

            // TODO: Named Parameters Support (specified in options)
            const params = arguments;

            return new Promise((resolve, reject) => {

                // Invoke the action with the specified context
                try {
                    const result = self.handler.apply(instance, params);

                    // Send notification that we succeeded
                    self.emit('success', result, instance);
                    return resolve(result);
                } catch (err) {
                    // Notify that we have failed
                    self.emit('failure', err, instance);

                    // Return a rejected promise
                    return reject(err);
                }
            });
        };
    }
}

/**
 * Function to create a new action
 * @param name
 * @param handler
 * @param options
 */
function createAction(name, handler, options) {

    options = options || {};
    options.parameters = options.parameters || functions.getParameterNames(handler);

    const action = new Action(name, handler, options);
    return action;
}

// ** Exports
module.exports = createAction;
module.exports.Action = Action;